$('.el08').on('change', function() {
	if($(this).is(':checked'))
	{
		$(this).parents('tr').addClass('sl');
	} else {
		$(this).parents('tr').removeClass('sl')
	}
});
var max_scroll = 600; // this is the scroll position to start positioning the nav in a fixed way
$(document).ready(function(){
        $(window).scroll(function () {
	        var navbar = $(".edit-navbar");
	        var scrollTop = document.documentElement.scrollTop || document.body.scrollTop;
	        if(scrollTop > max_scroll && !navbar.is(".navbar-fixed-top")) {
				navbar.addClass("navbar-fixed-top");
	        }
	        else if(scrollTop < max_scroll && navbar.is(".navbar-fixed-top") ) {
				navbar.removeClass("navbar-fixed-top");
	        }
		});
	});

$('#showModalEdit').click(function(event) {
	var total_check = 0;
	var firstId = 0;
	$('.el08').each(function(index, el) {
		if($(el).is(':checked')) {
			total_check += 1;
			if(firstId == 0) firstId = $(el).attr('id').split('_');
		}
	});
	if(total_check == 0) {
		alert('物件を選択して下さい。');
		return;
	}
	firstId = firstId[1];
	$('#inputEstate_name').val($('#e_'+firstId).text());
	if($('#a_'+firstId).text() != '') $('#inputEstate_cat').val($('#a_'+firstId).text());
	$('#inputEstate_add').val($('#r_'+firstId).text());
	$('#total_rows').text(total_check);
	$('#modalEdit').modal('show');
});

$('#checkAll').on('click', function(){
	if($(this).is(':checked'))
	{
		$('.el08').prop('checked','checked');
		$('.el08').parents('tr').addClass('sl');
	} else {
		$('.el08').removeProp('checked');
		$('.el08').parents('tr').removeClass('sl');
	}
});
$('#btnSaveEdit').click(function(event) {
	var ids = [];
	$('.el08').each(function(index, el) {
		if($(el).is(':checked')) {
			id = $(el).attr('id').split('_');
			ids.push(id[1]);
		}
	});
	var name = $('#inputEstate_name').val();
	var cat = $('#inputEstate_cat').val();
	var add = $('#inputEstate_add').val();
	$('#btnSaveEdit').attr('disabled', true);
	$.ajax({
		url: base_url + '/home/save-edit',
		type: 'POST',
		data: {ids: ids, name: name, cat: cat, add: add},
		success: function(res)
		{
			$('#btnSaveEdit').removeAttr('disabled');
			json = $.parseJSON(res);
			if(json.type == 'error')
			{
				$('#resModal').html('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'+json.mess+'</div> ');
			} else {
				$('#modalEdit').modal('hide');
				$.each(ids, function(index, val) {
					$('#e_'+val).html(name);
					$('#a_'+val).html(cat);
					$('#r_'+val).html(add);
				});
				$('.el08').removeProp('checked');
				$('.el08').parents('tr').removeClass('sl');
				$('#inputEstate_name').val('');
				$('#inputEstate_cat').val('賃貸物件');
				$('#inputEstate_add').val('');
			}
		}
	})
});

var _timeoutId = 0;
var selectedText = '';
var selectedId = '';
var _startHoldEvent = function(e) {
	id = e.attr('id');
	id = id.split('_');
	id = id[1];
	selectedText = $('#e_'+id).text();
	selectedId = id;
  _timeoutId = setInterval(function() {
     showSelectAll.call(e);
  }, 1000);
};

var _stopHoldEvent = function(e) {
	
  clearInterval(_timeoutId );
};

$('.el08').on('mousedown', function() {_startHoldEvent($(this));}).on('mouseup mouseleave', _stopHoldEvent);

var showSelectAll = function(e)
{
	console.log(selectedText);

	//if(confirm('Would you like set check to all value like this ?'))
	//{
		$('.el08').each(function(index, el) {
			id = $(el).attr('id');
			id = id.split('_');
			id = id[1];
			if($('#e_'+id).text() == selectedText)
			{
				$(el).prop('checked','checked');
				$(el).parents('tr').addClass('sl');
			}
		});
		$('#cb_'+selectedId).removeProp('checked');
	//}
}
var setForm = function(el)
{
	name = $(el).attr('id');
	col = name.split('_');
	col = col[1];
	val = $(el).val();
	$('#orderByCol').val(col);
	$('#orderByType').val(val);
	form1.submit();
}
$('#contact-form').on('submit', function(event) {
		$('#btn-send-contact').attr('disabled', 'disabled');
		$.ajax({
			url: base_url + '/home/send-contact',
			data: $('#contact-form').serialize(),
			type: 'POST',
			dataType: 'json',
			success: function(json)
			{
				$('#contact-result').html('<div class="alert alert-'+json.type+'">'+json.message+'</div>');
				if(json.type == 'success')
				{
					$('#c-name').val('');
					$('#c-email').val('');
					$('#c-message').val('');
					
					setTimeout(function(){ $('#modalContact').modal('hide'); }, 3000);
				} else {
					
				}
				$('#btn-send-contact').removeAttr('disabled');
			}
		});
		
		return false;
	});

$('.btn-scroll').click(function(event) {
	if($('.top-nav-ul').is(':visible')) {
		$('.top-nav-ul').slideUp('fast');
	} else {
		$('.top-nav-ul').slideDown('fast');
	}
	
});
