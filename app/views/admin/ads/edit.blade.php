@extends('layouts.admin')
@section('main')
	<h1 class="page-header">Edit Ads #{{ $ads->id }}</h1>
	<form action="{{ URL::to('admin/ads-manager/doEdit/'.$ads->id) }}" method="POST" class="form-horizontal" role="form">
		@include('includes.notifications')
		<div class="form-group">
			<label for="textareaCodes" class="col-sm-2 control-label">HTML Code:</label>
			<div class="col-sm-6">
				{{ Form::textarea('codes', $ads->code, array('id'=>'textareaCode', 'class'=>'form-control', 'rows'=>10, 'placeholder'=>'Ads code here') ) }}
			</div>
		</div>
		<div class="form-group">
			<label for="inputPosition" class="col-sm-2 control-label">Position:</label>
			<div class="col-sm-6">
				{{ Form::select('position', Config::get('app.ads_position'), $ads->position, array('id'=>'inputPosition', 'class'=>'form-control', 'required'=>'required') ) }}
			</div>
		</div>
			<div class="form-group">
				<div class="col-sm-10 col-sm-offset-2">
					<button type="submit" class="btn btn-primary">Save Changed</button>
				</div>
			</div>
	</form>
@stop