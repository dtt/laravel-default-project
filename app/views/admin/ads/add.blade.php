@extends('layouts.admin')
@section('main')
	<h1 class="page-header">Add new Ads</h1>
	<form action="{{ URL::to('admin/ads-manager/doAdd') }}" method="POST" class="form-horizontal" role="form">
		@include('includes.notifications')
		<div class="form-group">
			<label for="textareaCodes" class="col-sm-2 control-label">HTML Code:</label>
			<div class="col-sm-6">
				{{ Form::textarea('codes', '', array('id'=>'textareaCode', 'class'=>'form-control', 'rows'=>10, 'placeholder'=>'Ads code here') ) }}
			</div>
		</div>
		<div class="form-group">
			<label for="inputPosition" class="col-sm-2 control-label">Position:</label>
			<div class="col-sm-6">
				{{ Form::select('position', Config::get('app.ads_position'), 1, array('id'=>'inputPosition', 'class'=>'form-control', 'required'=>'required') ) }}
			</div>
		</div>
			<div class="form-group">
				<div class="col-sm-10 col-sm-offset-2">
					<button type="submit" class="btn btn-primary">Add Ads</button>
				</div>
			</div>
	</form>
@stop