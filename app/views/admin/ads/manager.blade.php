@extends('layouts.admin')
@section('main')
	<h1 class="page-header">Ads Manager</h1>
	@include('includes.notifications')
	<div class="table-responsive">
		<table class="table table-hover">
			<thead>
				<tr>
					<th>ID</th>
					<th>Position</th>
					<th>Added Date</th>
					<th>#</th>
				</tr>
			</thead>
			<tbody>
				@if(count($ads))
				@foreach($ads as $v)
				<tr>
					<td>{{ $v->id }}</td>
					<td>{{ Config::get('app.ads_position')[$v->position] }}</td>
					<td>{{ $v->created_at }}</td>
					<td><a href="{{ URL::to('/admin/ads-manager/edit/'.$v->id) }}" class="btn btn-sm btn-info">EDIT</a> <a href="{{ URL::to('/admin/ads-manager/delete/'.$v->id) }}" class="btn btn-sm btn-danger" onclick="return confirm('Do you want delete this ads ?');">DELETE</a></td>
				</tr>
				@endforeach
				@else 
				<tr>
					<td colspan="4">No Ads</td>
				</tr>
				@endif
			</tbody>
		</table>
	</div>
@stop