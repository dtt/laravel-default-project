@extends('layouts.admin')
@section('main')
	<h1 class="page-header">Diskspace</h1>
	<div class="progress">
	  <div class="progress-bar" role="progressbar" aria-valuenow="{{ $sv['percent_used'] }}" aria-valuemin="0" aria-valuemax="100" style="width: {{ $sv['percent_used'] }}%;">
	    {{ $sv['percent_used'] }}%
	  </div>
	</div>
	<ul>
		<li>Total Diskspace: {{ $sv['total_disk'] }}</li>
		<li>Free Diskspace: {{ $sv['free_disk'] }}</li>
		<li>Used: {{ $sv['used'] }}</li>
		<li>Available: {{ $sv['percent'] }}%</li>
	</ul>
@stop