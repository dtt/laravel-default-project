<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Admin Login</title>

		{{ HTML::style('assets/css/bootstrap.min.css') }}
		{{ HTML::style('assets/css/bootstrap.custom.css') }}
		{{ HTML::style('assets/css/admin-login.css') }}
	</head>
	<body>
		<h1 class="text-center">Airbnb Tool Login</h1>
		<div class="container">
			<div class="login-container">
		            <div id="output"></div>
		            <div id="avt" class="avatar"></div>
		            <div class="form-box">
		                <form action="" method="" onsubmit="adminLogin();return false;">
		                    <input name="user" type="text" placeholder="username">
		                    <input type="password" name="password" placeholder="password">
		                    <button class="btn btn-primary btn-block login" onclick="" type="submit">Login</button>
		                </form>
		            </div>
		        </div>
		        
		</div>

		<!-- jQuery -->
		{{ HTML::script('assets/js/jquery-1.11.1.js') }}
		{{ HTML::script('assets/js/bootstrap.min.js') }}
		<script type="text/javascript">
			var base_url = '{{ URL::to('/') }}';
				var username = $("input[name=user]");
				var password = $("input[name=password]");
	            var adminLogin = function() {
	                if (username.val() != "") {
	                	$('#avt').removeClass('avatar');
	                	$('#avt').addClass('avatar1');
	                	$.ajax({
	                		url: base_url + '/admin/doLogin',
	                		data: {username: username.val(), password: password.val()},
	                		type: 'POST',
	                		success: function(json)
	                		{
	                			if(json.has) {
		                			$("#output").addClass("alert alert-success animated fadeInUp").html("Welcome back " + "<span style='text-transform:uppercase'>" + username.val() + "</span>");
				                    $("#output").removeClass(' alert-danger');
				                    $("input").css({
				                    "height":"0",
				                    "padding":"0",
				                    "margin":"0",
				                    "opacity":"0"
				                    });
				                    //change button text 
				                    $('button[type="submit"]').html("continue")
				                    .removeClass("btn-info")
				                    .addClass("btn-default").click(function(){
				                    $("input").css({
				                    "height":"auto",
				                    "padding":"10px",
				                    "opacity":"1"
				                    }).val("");
				                    });
				                    $('.btn-block').remove();
			                    	window.setTimeout(function() {location.href = base_url + '/admin';}, 1000);
			                    	return false;
			                    } else {
			                    	$("#output").removeClass(' alert alert-success');
	                    			$("#output").addClass("alert alert-danger animated fadeInUp").html("Username or Password does not match.");
	                    			$('#avt').addClass('avatar');
	                				$('#avt').removeClass('avatar1');
	                				return false;
			                    }
	                		},
	                		error: function(res) {

	                		}
	                	});
	                    return false;
	                    
	                } else {
	                    //remove success mesage replaced with error message
	                    $("#output").removeClass(' alert alert-success');
	                    $("#output").addClass("alert alert-danger animated fadeInUp").html("sorry enter a username ");
	                    return false;
	                }
	                //console.log(textfield.val());

	            };

		</script>
	</body>
</html>