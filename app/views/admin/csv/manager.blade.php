@extends('layouts.admin')
@section('main')
	<h1 class="page-header">File Manager</h1>
	@include('includes.notifications')
	<form action="{{ URL::to('admin/del-csv') }}" method="POST">
	<button type="submit" class="btn btn-danger" onclick="return confirm('Do you want delete files ?');">Delete Selected</button>
	<div class="table-responsive">
		<table class="table table-hover">
			<thead>
				<tr>
					<th><div class="checkbox d-checkbox">
							<label>
								<input id="checkAll" type="checkbox" value="">
							</label>
						</div></th>
					<th>File Name</th>
					<th>Upload Date</th>
					<th>#</th>
				</tr>
			</thead>
			<tbody>
				@if(count($files_upload))
				@foreach($files_upload as $k=>$v)
				<tr>
					<td><div class="checkbox d-checkbox">
							<label>
								<input type="checkbox" class="child-cb" name="child-cb[]" value="{{ $v['file_name'] }}">
							</label>
						</div>
					</td>
					<td>{{ $v['new_fname'] }}</td>
					<td>{{ $v['date_day'] }}</td>
					<td><a href="{{ URL::to('/admin/csv-del/'.$v['file_name']) }}" class="btn btn-sm btn-danger" onclick="return confirm('Do you want delete this file ?');">DELETE</a></td>
				</tr>
				@endforeach
				@else 
				<tr>
					<td colspan="4">No File found !</td>
				</tr>
				@endif
			</tbody>
		</table>
		{{ $paginate }}
	</div>
	</form>
@stop