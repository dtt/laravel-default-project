@extends('layouts.admin')
@section('main')
	<h1 class="page-header">CSV Download</h1>
	<form action="" method="POST" class="form-horizontal" role="form">
		<div class="form-group">
			<label for="input" class="col-sm-2 control-label">Data date:</label>
			<div class="col-sm-2">
				<select name="dateY" id="input" class="form-control" required="required">
					<?php $startY = 2016;
					$endY = date('Y');
					$select = ''; ?>
					@for($i = $startY; $i <= $endY; $i++)
						<option value="{{ $i }}">{{ $i }}</option>
					@endfor
				</select>
			</div>
			<div class="col-sm-2">
				<select name="dateM[]" id="input" class="form-control" required="required" multiple>					@foreach(range(1,12) as $i )
						<option value="{{ $i }}">{{ $i }}</option>
					@endforeach
				</select>
			</div>
		</div>
			<div class="form-group">
				<div class="col-sm-10 col-sm-offset-2">
					<button type="submit" class="btn btn-primary">Download</button>
				</div>
			</div>
	</form>
@stop