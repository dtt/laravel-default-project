
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	
	<title>@yield('title') Airbnb CSV Admin</title>
	{{ HTML::style('assets/admin/css/bootstrap.min.css') }}
	{{ HTML::style('assets/admin/css/metisMenu.min.css') }}
	{{ HTML::style('assets/admin/css/sb-admin-2.css') }}
	{{ HTML::style('assets/admin/css/font-awesome.min.css') }}

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->

</head>

<body>

	<div id="wrapper">

		<!-- Navigation -->
		<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="{{ URL::to('/admin') }}">Airbnb CSV</a>
			</div>
			<!-- /.navbar-header -->

			<ul class="nav navbar-top-links navbar-right">
				<!-- /.dropdown -->
				<li class="dropdown">
					<a class="dropdown-toggle" data-toggle="dropdown" href="#">
						<i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
					</a>
					<ul class="dropdown-menu dropdown-user">
						
						<li><a href="{{ URL::to('admin/logout') }}"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
						</li>
					</ul>
					<!-- /.dropdown-user -->
				</li>
				<!-- /.dropdown -->
			</ul>
			<!-- /.navbar-top-links -->

			<div class="navbar-default sidebar" role="navigation">
				<div class="sidebar-nav navbar-collapse">
					<ul class="nav" id="side-menu">
						<li>
							<a href="{{ URL::to('admin') }}"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
						</li>
						<li>
							<a href="{{ URL::to('admin/ads-manager') }}"><i class="fa fa-bar-chart-o fa-fw"></i> Ads Manager<span class="fa arrow"></span></a>
							<ul class="nav nav-second-level">
								<li>
									<a href="{{ URL::to('admin/ads-manager') }}">Ads</a>
								</li>
								<li>
									<a href="{{ URL::to('admin/ads-manager/add') }}">Add New</a>
								</li>
							</ul>
						</li>
						<li>
							<a href="{{ URL::to('admin/csv') }}"><i class="fa fa-bar-chart-o fa-fw"></i> CSV<span class="fa arrow"></span></a>
							<ul class="nav nav-second-level">
								<li>
									<a href="{{ URL::to('admin/csv') }}">現在のCSVの件数</a>
								</li>
								<li>
									<a href="{{ URL::to('admin/csv-download') }}">各月/年単位でのCSVデータダウンロード機能</a>
								</li>
							</ul>
						</li>
						<li>
							<a href="{{ URL::to('admin/diskspace') }}"><i class="fa fa-dashboard fa-fw"></i> サーバーの空き容量</a>
						</li>
					</ul>
				</div>
				<!-- /.sidebar-collapse -->
			</div>
			<!-- /.navbar-static-side -->
		</nav>

		<!-- Page Content -->
		<div id="page-wrapper">
			<div class="container-fluid">
				<div class="row">
					<div class="col-lg-12">
						@yield('main')
					</div>
					<!-- /.col-lg-12 -->
				</div>
				<!-- /.row -->
			</div>
			<!-- /.container-fluid -->
		</div>
		<!-- /#page-wrapper -->

	</div>
	<!-- /#wrapper -->
	{{ HTML::script('assets/admin/js/jquery.min.js') }}
	{{ HTML::script('assets/admin/js/bootstrap.min.js') }}
	{{ HTML::script('assets/admin/js/metisMenu.min.js') }}
	{{ HTML::script('assets/admin/js/sb-admin-2.js') }}

</body>

</html>
