<!DOCTYPE html>
<html lang="ja">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>@yield('title') Airbnb CSV Editor</title>
		<meta name="keywords" content="Airbnb,csv,編集,確定申告,不動産収入の内訳所,所得の内訳所,無料">
		<meta name="description" content="AirbnbからダウンロードしたCSVをWeb上で編集するWebツールです。物件名、物件の種類（賃貸物件／自己所有物件）、物件の住所を記載する事ができます。">
		<!-- Bootstrap CSS -->
		 <link rel="shortcut icon" href="{{{ asset('assets/img/favicon.png') }}}">
		{{ HTML::style('assets/css/bootstrap.min.css') }}
		{{ HTML::style('assets/css/bootstrap.custom.css') }}
		{{ HTML::style('assets/css/style.css') }}
		@yield('style')
		<script>
		var base_url = '{{ URL::to('/') }}';
		</script>
		<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		  ga('create', 'UA-74383512-1', 'auto');
		  ga('send', 'pageview');

		</script>
	</head>
	<body>

	<div class="container">
		<div class="row">
		</div>
	</div>
	<h1 class="text-center" id="home-text"><a href="{{ URL::to('/') }}">Airbnb CSV Editor</a>
	<button class="btn btn-primary btn-scroll">
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
	</button>
	<ul class="top-nav-ul">
		<li><a href="{{ URL::to('home/change-log') }}">更新履歴</a></li>
		<li><a href="{{ URL::to('home/guide') }}">ユーザーガイド</a></li>
	</ul>
	</h1>
	<div class="container">
			@yield('main')
		</div>
	</div>
		

		<!-- jQuery -->
		{{ HTML::script('assets/js/jquery-1.11.1.js') }}
		<!-- Bootstrap JavaScript -->
		{{ HTML::script('assets/datatable/js/datatables.min.js') }}
		{{ HTML::script('assets/js/bootstrap.min.js') }}

		{{ HTML::script('assets/js/custom.js') }}
		@yield('script')
	</body>
</html>