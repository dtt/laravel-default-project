<br>
{{ $name }}様<br>
<br>
この度はAirbnb CSV Editorへご要望をお送り頂き誠にありがとうございます。<br>
下記の内容にて承らせて頂きました。<br>
＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿<br>
お問い合わせ番号：{{ date('Y-m-d') }}<br>
<br>
{{ $messages }}<br>
＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿<br>
<br>
皆様とってより良いサービスをご提供出来るよう活用させて頂きます。<br>
<br>
<br>
<br>
<br>
┏━━━━━Airbnb CSV Editor━━━━━━┓<br>
　　http://brows.xsrv.jp/home<br>
┗━━━━━━━━━━━━━━━━━━━┛<br>