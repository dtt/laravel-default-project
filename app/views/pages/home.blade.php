@extends('layouts.default')

@section('main')
</div>
</div>
<h1 class="text-center" id="home-text">Airbnb CSV Editor
<button class="btn btn-primary btn-scroll">
	<span class="icon-bar"></span>
	<span class="icon-bar"></span>
	<span class="icon-bar"></span>
</button>
<ul class="top-nav-ul">
	<li><a href="{{ URL::to('home/change-log') }}">更新履歴</a></li>
	<li><a href="{{ URL::to('home/guide') }}">ユーザーガイド</a></li>
</ul>
</h1>
<div class="container">
	<div class="row">
	<div class="col-xs-12">
		<div class="col-sm-8 col-sm-offset-2">
			<div class="text-center">
				<h2>About "Airbnb CSV Editor"</h2>
				<p>AirbnbからダウンロードしたCSVをWeb上で編集するWebツールです。<br>物件名、物件の種類（賃貸物件／自己所有物件）、物件の住所を記載する事ができます。</p><br><p>現在、無料にて公開中です。Airbnb CSV Editorについてのご要望などございましたら下記へご連絡願います。</p>
			<a class="btn btn-host" id="btn-modal-contact" data-toggle="modal" href='#modalContact'>お問い合わせ先</a>
		</div>
	</div>
	<div class="col-sm-8 col-sm-offset-2">
		<div id="uploadfile" class="panel panel-default">
		<div class="panel-body">
			<div class="text-center">
				{{ @$ads['top'] }}
			</div>
			{{ Form::open(array('method'=>'POST', 'class'=>'form-horizontal', 'files'=>true)) }}
				@include('includes.notifications')
				<div class="form-group">
					<label for="" class="col-sm-4 control-label">お名前</label>
					<div class="col-sm-8">
						{{ Form::text('username', '', array('class'=>'form-control', 'required', 'maxlength' => 30) ) }}
					</div>
				</div>
				<div class="form-group">
					<label for="" class="col-sm-4 control-label">住所</label>
					<div class="col-sm-8">
						{{ Form::text('address', '', array('class'=>'form-control', 'required') ) }}
					</div>
				</div>
				<div class="form-group">
					<label for="" class="col-sm-4 control-label">E-mail</label>
					<div class="col-sm-8">
						{{ Form::text('email', '', array('class'=>'form-control', 'required') ) }}
					</div>
				</div>
				<div class="form-group">
					<label for="inputCsv" class="col-sm-4 control-label">AirbnbからダウンロードしたCSVファイル</label>
					<div class="col-sm-8">
						{{ Form::file('file', '', array('class'=>'form-control') ) }}
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-6 col-sm-offset-3">
						<button type="submit" id="btn-upload" class="btn btn-primary btn-block btn-lg">アップロード</button>
					</div>
					<div class="text-center">
						
					</div>
				</div>
			</form>
			<div class="text-center">
				{{ @$ads['bottom'] }}
			</div>
		</div>
	</div>
		
	</div>
	</div>


	<div class="modal fade" id="modalContact">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">お問い合わせ先</h4>
				</div>
				<div class="modal-body">
					<form id="contact-form" action="" method="POST" class="form-horizontal" role="form">
						<div id="contact-result"></div>
						<div class="form-group">
							<label for="inputName" class="col-sm-2 control-label">名前</label>
							<div class="col-sm-10">
								<input type="text" name="name" id="c-name" class="form-control" required="required">
							</div>
						</div>
						<div class="form-group">
							<label for="email" class="col-sm-2 control-label">E-mail</label>
							<div class="col-sm-10">
								<input type="text" name="email" id="c-email" class="form-control" required="required">
							</div>
						</div>
						<div class="form-group">
							<label for="subject" class="col-sm-2 control-label">タイトル</label>
							<div class="col-sm-10">
								<select name="title" id="c-title" class="form-control" required="required">
									<option value="不具合についてのご報告">不具合についてのご報告</option>
									<option value="サービスについてのご要望">サービスについてのご要望</option>
									<option value="その他">その他</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label for="subject" style="padding-right: 0" class="col-sm-2 control-label">メッセージ</label>
							<div class="col-sm-10">
								<textarea name="message" id="c-message" class="form-control" rows="5" required="required"></textarea>
							</div>
						</div>
						<div class="form-group">
						<div class="text-center">
							<button type="button" class="btn btn-default" data-dismiss="modal">キャンセル</button>
							<button type="submit" id="btn-send-contact" class="btn btn-primary">送信</button>
						</div>
						</div>
					</form>
				</div>
				
			</div>
		</div>
	</div>


@stop
@section('style')
	<style type="text/css">
		
		.form-horizontal .control-label {
			text-align: left;
		}
		.panel-body {padding: 40px;}
		.form-group:last-child {margin-bottom: 0;}
		#btn-upload {padding: 10px 16px;}
		a#btn-modal-contact {font-size: 20px;}
	</style>
@stop

@section('script')
	<script type="text/javascript">
	var base_url = '{{ URL::to('/') }}';
	
	</script>
@stop