@extends('layouts.default')
@section('title') 編集画面｜ @stop
@section('main')
</div>
</div>
<h1 class="text-center" id="home-text">Airbnb CSV Editor
<button class="btn btn-primary btn-scroll">
	<span class="icon-bar"></span>
	<span class="icon-bar"></span>
	<span class="icon-bar"></span>
</button>
<ul class="top-nav-ul">
	<li><a href="{{ URL::to('home/change-log') }}">更新履歴</a></li>
	<li><a href="{{ URL::to('home/guide') }}">ユーザーガイド</a></li>
</ul>
</h1>
<div class="container">
	<div class="row">
	<div class="col-xs-12">
		<h1 class="text-center" id="page-name">CSV編集画面</h1>
		<br>
	<div class="row">
		<div class="col-sm-8 col-sm-offset-2 text-des">
			<h2>編集方法</h2>
			<ol>
			<li>編集したい行の<b>【物件名】</b>左横にあるチェックボックスをクリックします。</li>
			<li><b>【①ＣＳＶデータを編集】</b>ボタンをクリックします。</li>
			<li><b>【リスティング】</b>の項目に物件名を入力します。</li>
			<li><b>【物件種別】</b>の項目で"賃貸物件（アパート／マンション／貸家）"又は"自己所有"を選択します。</li>
			<li><b>【物件の住所】</b>の項目に物件の住所を入力します。</li>
			<li><b>【編集完了】</b>ボタンを押下します。</li>
			<li>複数のチェックボックスにチェックを入れることで、一括編集も可能です。</li>
			<li>編集作業を終えた後、<b>②【不動産収入の内訳所】</b>をダウンロードボタン又は<b>③【所得の内訳所】</b>をダウンロードボタンを押下します。</li>
			</ol>
			<p>※このシステムはベータ版（テスト公開）です。<br>不具合・ご要望など御座いましたら下記お問い合わせ先へご連絡頂けますと幸いです。</p>
			<br>
			
		</div>
		<div class="col-sm-2"></div>
	</div>
	<div class="text-center"><a class="btn btn-host" id="btn-modal-contact" data-toggle="modal" href='#modalContact'>お問い合わせ先</a></div>
	<div class="row">
		<div class="col-xs-6">
			<table class="table table-hover table-bordered">
				<tbody>
					<tr>
						<td class="col-sm-4">お名前</td>
						<td><span style="font-weight: bold;">{{ Session::get('username') }}</span></td>
					</tr>
					<tr>
						<td>e-mail</td>
						<td><span style="font-weight: bold;">{{ Session::get('email') }}</span></td>
					</tr>
					<tr>
						<td>住所</td>
						<td><span style="font-weight: bold;">{{ Session::get('address') }}</span></td>
					</tr>
				</tbody>
			</table>
		</div>
		<form action="{{ URL::to('/home/set-col') }}" method="POST" role="form">
		<div class="col-xs-6">
			<table class="table table-condensed borderless table-bordered">
				<tbody>
					<tr>
						<td>
							<div class="checkbox">
								<label>
									<input name="col[]" type="checkbox" @if(in_array('col3', Session::get('tableCol'))) checked="checked" @endif value="col3">
									確認コード
								</label>
							</div>
						</td>
						<td>
							<div class="checkbox">
								<label>
									<input name="col[]" type="checkbox" @if(in_array('col5', Session::get('tableCol'))) checked="checked" @endif value="col5">
									夜
								</label>
							</div>
						</td>
						<td rowspan="4" class="verticalalign">
							<button type="submit" class="btn btn-primary">非表示状態の列を表示</button>
						</td>
					</tr>
					<tr>
						<td>
							<div class="checkbox">
								<label>
									<input name="col[]" type="checkbox" @if(in_array('col11', Session::get('tableCol'))) checked="checked" @endif value="col11">
									詳細
								</label>
							</div>
						</td>
						<td>
							<div class="checkbox">
								<label>
									<input name="col[]" type="checkbox" @if(in_array('col12', Session::get('tableCol'))) checked="checked" @endif value="col12">
									紹介文
								</label>
							</div>
						</td>
					</tr>
					<tr>
						<td>
							<div class="checkbox">
								<label>
									<input name="col[]" type="checkbox" @if(in_array('col15', Session::get('tableCol'))) checked="checked" @endif value="col15">
									送金済
								</label>
							</div>
						</td>
						<td>
							<div class="checkbox">
								<label>
									<input name="col[]" type="checkbox" @if(in_array('col16', Session::get('tableCol'))) checked="checked" @endif value="col16">
									ホスト不在
								</label>
							</div>
						</td>
					</tr>
					<tr>
						<td>
							<div class="checkbox">
								<label>
									<input name="col[]" type="checkbox" @if(in_array('col17', Session::get('tableCol'))) checked="checked" @endif value="col17">
									清掃料金
								</label>
							</div>
						</td>
						<td>&nbsp;</td>
						
					</tr>
				</tbody>
			</table>
		</div>
		
		</form>
	</div>
	</div>
</div> <!-- end .row -->
</div> <!-- end .container -->
<hr>
<!-- Fixed navbar -->
    <nav class="navbar navbar-default edit-navbar">
      <div class="container">
        <div id="navbar" class="collapse navbar-collapse navbar-ex1-collapse">
          <ul class="nav navbar-nav">
          	
          	<li><a data-toggle="modal" href='#modalEdit' class="btn btn-primary">①ＣＳＶデータを編集</a></li>
            <li><a href="{{ URL::to('/home/download-xlsx/1') }}" class="btn btn-host btn-primary1">【不動産収入の内訳所】をダウンロード</a></li>
            <li><a href="{{ URL::to('/home/download-xlsx/2') }}" class="btn btn-host btn-primary1">③【所得の内訳所】をダウンロード</a></li>
            <li><a href="{{ URL::to('/home/undoall/') }}" onclick="return confirm('Would you like Remove All data and reupload file ?');" class="btn-down1 btn btn-info">編集前の状態に戻す</a></li>
            
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>
    <form action="" name="form1" method="GET" role="form">
    	<input type="hidden" name="orderByCol" id="orderByCol" value="">
    	<input type="hidden" name="orderByType" id="orderByType" value="">
    </form>
    <div id="page-content">
		<table class="table table-bordered edit-data" id="myTable">
			<thead>
				<tr>
					<th>日付</th>
					<th>種別</th>
					@if(in_array('col3', Session::get('tableCol')))<th>確認コード</th>@endif
					<th>開始日</th>
					@if(in_array('col5', Session::get('tableCol')))<th>夜</th>@endif
					<th>ゲスト</th>
					<th><input type="checkbox" class="el08n" id="checkAll"></th>
					<th>物件名</th>
					<th>物件種別</th>
					<th>物件の住所</th>
					@if(in_array('col11', Session::get('tableCol')))<th>詳細</th>@endif
					@if(in_array('col12', Session::get('tableCol')))<th>紹介文</th>@endif
					<th>通貨</th>
					<th>額</th>
					@if(in_array('col15', Session::get('tableCol')))<th>送金済</th>@endif
					@if(in_array('col16', Session::get('tableCol')))<th>ホスト不在</th>@endif
					@if(in_array('col17', Session::get('tableCol')))<th>清掃料金</th>@endif
				</tr>
				
			</thead>
			<tbody>
				@foreach($csvData as $k=>$v)
				<tr @if($v['cate'] == '予約') class="rs" @endif >
					<td>{{ $v['date'] }}</td>
					<td>{{ $v['cate'] }}</td>
					@if(in_array('col3', Session::get('tableCol')))<td>{{ $v['code'] }}</td>@endif
					<td>{{ $v['start'] }}</td>
					@if(in_array('col5', Session::get('tableCol')))<td>{{ $v['num'] }}</td>@endif
					<td>{{ $v['name'] }}</td>
					<td>@if($v['cate'] == '予約')
						<input type="checkbox" class="el08" id="cb_{{$v['id']}}">
						@endif </td>
					<td id="e_{{$v['id']}}">{{ $v['estate'] }}</td>
					<td id="a_{{$v['id']}}">{{ $v['ecate'] }}</td>
					<td id="r_{{$v['id']}}">{{ $v['eadd'] }}</td>
					@if(in_array('col11', Session::get('tableCol')))<td>{{ $v['detail'] }}</td>@endif
					@if(in_array('col12', Session::get('tableCol')))<td>{{ $v['num8'] }}</td>@endif
					<td>{{ $v['currency'] }}</td>
					<td>{{ $v['price'] }}</td>
					@if(in_array('col15', Session::get('tableCol')))<td>{{ $v['pay'] }}</td>@endif
					@if(in_array('col16', Session::get('tableCol')))<td>{{ $v['lost'] }}</td>@endif
					@if(in_array('col17', Session::get('tableCol')))<td>{{ $v['fee'] }}</td>@endif
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
	</div>

<div class="modal fade" id="modalEdit">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">①Edit/データ編集</h4>
			</div>
			<div class="modal-body">
				<form action="" method="POST" class="form-horizontal" role="form" onsubmit="return validateSave();">
					下記の項目を編集し、<b>編集完了</b>ボタンを押下して下さい。
					<div id="resModal"></div>
					<div class="form-group">
						<label for="inputEstate_name" class="col-sm-3 control-label">リスティング:</label>
						<div class="col-sm-9">
							<input type="text" name="estate_name" id="inputEstate_name" class="form-control" value="">
						</div>
					</div>
					<div class="form-group">
						<label for="inputEstate_cat" class="col-sm-3 control-label">物件種別:</label>
						<div class="col-sm-9">
							<select name="estate_cat" id="inputEstate_cat" class="form-control">
								<optgroup label="賃貸物件">
									<option value="アパート">アパート</option>
									<option value="マンション">マンション</option>
									<option value="貸家">貸家</option>
								</optgroup>
								<option value="自己所有">自己所有</option>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label for="inputEstate_add" class="col-sm-3 control-label">物件の住所:</label>
						<div class="col-sm-9">
							<input type="text" name="estate_add" id="inputEstate_add" class="form-control" value="">
						</div>
					</div>
						<div class="form-group">
							<div class="col-sm-9 col-sm-offset-3">
								<button type="button" id="btnSaveEdit" class="btn btn-primary">編集完了</button>
								<button type="button" class="btn btn-default" data-dismiss="modal">キャンセル</button>
							</div>
						</div>
				</form>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="modalContact">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">お問い合わせ先</h4>
				</div>
				<div class="modal-body">
					<form id="contact-form" action="" method="POST" class="form-horizontal" role="form">
						<div id="contact-result"></div>
						<div class="form-group">
							<label for="inputName" class="col-sm-2 control-label">名前</label>
							<div class="col-sm-10">
								<input type="text" name="name" id="c-name" class="form-control" value="{{ Session::get('username') }}" required="required">
							</div>
						</div>
						<div class="form-group">
							<label for="email" class="col-sm-2 control-label">E-mail</label>
							<div class="col-sm-10">
								<input type="text" name="email" id="c-email" class="form-control" value="{{ Session::get('email') }}" required="required">
							</div>
						</div>
						<div class="form-group">
							<label for="subject" class="col-sm-2 control-label">タイトル</label>
							<div class="col-sm-10">
								<select name="title" id="c-title" class="form-control" required="required">
									<option value="不具合についてのご報告">不具合についてのご報告</option>
									<option value="サービスについてのご要望">サービスについてのご要望</option>
									<option value="その他">その他</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label for="subject" style="padding-right: 0" class="col-sm-2 control-label">メッセージ</label>
							<div class="col-sm-10">
								<textarea name="message" id="c-message" class="form-control" rows="5" required="required"></textarea>
							</div>
						</div>
						<div class="form-group">
						<div class="text-center">
							<button type="button" class="btn btn-default" data-dismiss="modal">キャンセル</button>
							<button type="submit" id="btn-send-contact" class="btn btn-primary">送信</button>
						</div>
						</div>
					</form>
				</div>
				
			</div>
		</div>
	</div>

@stop

@section('style')
	{{ HTML::style('assets/datatable/css/datatables.min.css') }}
@stop

@section('script')

<script type="text/javascript">
var i = 4;
@if(in_array('col3', Session::get('tableCol'))) i += 1; @endif;
@if(in_array('col5', Session::get('tableCol'))) i += 1; @endif;
$('#myTable').DataTable({
	'pageLength': -1,
	"columnDefs": [ { "targets": i, "orderable": false } ],
	"order": [[ 0, "desc" ]],
	"language": {
        "url": "{{ asset('/assets/js/Japanese.json') }}"
    }
});

</script>
@stop