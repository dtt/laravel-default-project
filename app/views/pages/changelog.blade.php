@extends('layouts.main')
@section('title') 更新履歴 | @stop
@section('main')

	<div class="row">
	<div class="col-xs-12">
		<h2 class="version-no">Version 1.0.1</h2>
		<p class="version-date">16/06/2016</p>
		<ul>
			<li>Added Change Log.</li>
			<li>Added User Guide.</li>
			<li>Modified layout style</li>
			<li>Fixed some bugs</li>
		</ul>
		<h2 class="version-no">Version 1.0.0</h2>
		<p class="version-date">16/04/2016</p>
		<ul>
			<li>Added Home page</li>
			<li>Added upload CSV function, edit CSV which uploaded. Download CSV after edited successfully</li>
		</ul>
	</div>
	</div>

@stop
