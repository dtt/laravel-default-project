@extends('layouts.default')
@section('title') 編集画面｜ @stop
@section('main')
	<div class="col-xs-12">
		<h1 class="text-center">Export</h1>
		<br>
	</div>
</div> <!-- end .row -->
</div> <!-- end .container -->
<!-- Fixed navbar -->
    <nav class="navbar navbar-default edit-navbar">
      <div class="container">
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
          	<li><a class="btn btn-info" href="{{ URL::to('/home/download-xlsx/1') }}" target="_blank">Save to XLS</a></li>
            
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>
		<table class="table table-bordered table-vcenter edit-data" id="myTable">
			<thead>
				<tr>
					<th rowspan="3">貸家 貸地 等の別</th>
					<th rowspan="3">用途（住宅用、 住宅用以 外等の別）</th>
					<th rowspan="3">不動産の所在地</th>
					<th rowspan="3">賃借人の住所・氏名</th>
					<th rowspan="3">賃貸契約期間</th>
					<th rowspan="3">貸付面積</th>
					<th colspan="4">本年中の収入金額</th>
					<th rowspan="3">補償金 敷金（期末残高）</th>
				</tr>
				<tr>
					<th colspan="2">賃貸料</th>
					<th rowspan="2">礼金 権利金 更新料</th>
					<th rowspan="2">名義書換料 その他</th>
				</tr>
				<tr>
					<th>月額</th>
					<th>年額</th>
				</tr>
			</thead>
			<tbody>
				@foreach($csvData as $v)
				<tr>
					<td>{{ $v['col1'] }}</td>
					<td>{{ $v['col2'] }}</td>
					<td>{{ str_replace("\n", '<br>', $v['col3']) }}</td>
					<td>{{ $v['col4'] }}</td>
					<td>{{ $v['col5'] }}</td>
					<td>{{ $v['col6'] }}</td>
					<td>{{ $v['col7'] }}</td>
					<td class="money">{{ $v['col8'] }}</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				@endforeach
			</tbody>
			<tfoot>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td id="totalMoney">0</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
			</tfoot>
		</table>
	</div>

<div class="modal fade" id="modalEdit">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">①Edit/データ編集</h4>
			</div>
			<div class="modal-body">
				<form action="" method="POST" class="form-horizontal" role="form" onsubmit="return validateSave();">
					下記の項目を編集し、<b>編集完了</b>ボタンを押下して下さい。
					<div id="resModal"></div>
					<div class="form-group">
						<label for="inputEstate_name" class="col-sm-3 control-label">リスティング:</label>
						<div class="col-sm-9">
							<input type="text" name="estate_name" id="inputEstate_name" class="form-control" value="">
						</div>
					</div>
					<div class="form-group">
						<label for="inputEstate_cat" class="col-sm-3 control-label">物件種別:</label>
						<div class="col-sm-9">
							<select name="estate_cat" id="inputEstate_cat" class="form-control">
								<optgroup label="賃貸物件">
									<option value="アパート">アパート</option>
									<option value="マンション">マンション</option>
									<option value="3">3</option>
								</optgroup>
								<option value="自己所有">自己所有</option>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label for="inputEstate_add" class="col-sm-3 control-label">物件の住所:</label>
						<div class="col-sm-9">
							<input type="text" name="estate_add" id="inputEstate_add" class="form-control" value="">
						</div>
					</div>
						<div class="form-group">
							<div class="col-sm-9 col-sm-offset-3">
								<button type="button" id="btnSaveEdit" class="btn btn-primary">編集完了</button>
								<button type="button" class="btn btn-default" data-dismiss="modal">キャンセル</button>
							</div>
						</div>
				</form>
			</div>
		</div>
	</div>
</div>
@stop

@section('style')
	<!--{{ HTML::style('assets/datatable/css/datatables.min.css') }}-->
	<style type="text/css">
	.table-vcenter td {
   vertical-align: middle!important;
}
	table.table-bordered.dataTable th:last-child, table.table-bordered.dataTable th:last-child, table.table-bordered.dataTable td:last-child, table.table-bordered.dataTable td:last-child {border-right-width: 1px;}
	</style>
@stop

@section('script')

<script type="text/javascript">
/*
$('#myTable').DataTable({
	'pageLength': -1,
	//"columnDefs": [ { "targets": 6, "orderable": false } ],
	"order": [[ 0, "desc" ]]
});
*/
var totalMoney = 0;
$('.money').each(function(index, el) {
	totalMoney += parseInt($(el).text());
});
$('#totalMoney').text(totalMoney);
</script>
@stop