<?php 
namespace App\DTT\Paginate;

class FilePagination {
	public static function make($currentPage, $totalPage, $links, $numLink = 2)
	{
		$html = '<ul class="pagination">';
		for ($i=1; $i <= $totalPage; $i++) { 
			$url = sprintf($links, $i);
			if($i == 1) {
				if($currentPage == 1) {
					$html .= '<li class=""><a href="#">&laquo;</a></li>';
				} elseif($i==1) {
					$html .= '<li><a href="'.$url.'">&laquo;</a></li>';
				}
			}
			if( ($i <= ($currentPage+$numLink)) && ($i >= ($currentPage-$numLink)) ) {
				if($currentPage == $i) {
					$html .= '<li class="active"><a href="#">'.$i.'</a></li>';
				} else {
					$html .= '<li><a href="'.$url.'">'.$i.'</a></li>';
				}
			}
			
			if($i == $totalPage) {
				if($currentPage == $totalPage) {
					$html .= '<li class=""><a href="#">&raquo;</a></li>';
				} else {
					$html .= '<li><a href="'.$url.'">&raquo;</a></li>';
				}
			}
		}
		$html .= '</ul>';
		return $html;
	}
}