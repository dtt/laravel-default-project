<?php

class Ads extends \Eloquent {
	protected $fillable = ['code', 'position'];
	protected $table = 'ads';
}