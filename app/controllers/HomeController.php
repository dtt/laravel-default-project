<?php
class HomeController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function getIndex()
	{
		$ads_place = Ads::whereIn('position', array(1,2) )->get();
		$ads = array();
		foreach ($ads_place as $key => $value) {
			if($value->position == 1) $ads['top'] = $value->code;
			if($value->position == 2) $ads['bottom'] = $value->code;
		}
		return View::make('pages.home', compact('ads'));
	}
	private function getRates()
	{
		$rateDir = Config::get('app.currency_path');
		$updateDir = Config::get('app.date_update_path');
		$content = File::get($rateDir);
		$flag = 1;
		if($content)
		{
			$updateDate = File::get($updateDir);
			if(strlen($updateDate) >= 8 && strlen($updateDate) <= 14)
			{
				$oldDay = (time() - strtotime($updateDate)) / 86400;
				if($oldDay <= 30)
				{
					$flag = 0;
				}
			}
		}
		//chuyen doi thanh array
		$tmp = explode("\r\n", $content);
		$arr = array();
		foreach ($tmp as $val) {
			$line = explode('|', $val);
			if(count($line) != 2) continue;
			$arr[$line[0]] = $line[1];
		}
		$content = $arr;
		if($flag == 1)
		{
			$page = @file_get_contents('http://ecodb.net/exchange/usd_jpy.html');
			if(!$page) return $content;
			if(!stristr($page, 'data.addRows([')) return $content;
			$fn = new FunctionController;
			$rateData = $fn->get2Str('data.addRows([', ']);', $page);
			$rates = array();
			if(preg_match_all('/\'(\d{4}\/\d{2})\',\s(\d{2,3}.\d{1,4})/', $rateData, $match))
			{
				if(count($match[1]) == count($match[2]))
				{
					foreach ($match[1] as $k=>$val) {
						$rates[$val] = $match[2][$k];
					}
				} else return $content;
				//cho nay luu file
				$saveData = "";
				foreach ($rates as $k=>$v) {
					$saveData .= $k . "|" . $v . "\r\n";
				}
				File::put(Config::get('app.currency_path'), $saveData);
				File::put(Config::get('app.date_update_path'), date('Y-m-d') );
				return $rates;
			}
			die();
		} else {
			return $content;
		}
	}
	public function postIndex()
	{

		if(Input::file())
		{
			$rules = array( 
				'file' => 'required|mimes:csv,txt',
				'username' => 'required',
				'address' => 'required',
				'email' => 'required|email');
			$message = array(
				'required'	=> 'Please select CSV file !',
				'mimes'	=> 'Allow only *.csv file !'
				);
			$validator = Validator::make(Input::all(), $rules, $message);
			if($validator->fails())
			{
				return Redirect::back()->withInput()->withErrors($validator);
			} else {
				$extension = Input::file('file')->getClientOriginalExtension();
				$fileName = base64_encode(Input::get('username')) . '_' . date('Y-m-d_H-i-s') . '.' . $extension;
				$file = Input::file('file')->move(Config::get('app.upload_dir'), $fileName);
				if($file)
				{
					Session::put('username', Input::get('username'));
					Session::put('address', Input::get('address'));
					Session::put('email', Input::get('email'));
					Session::put('filename', $fileName);
					$data = $this->getDataFromCSV(Config::get('app.upload_dir') . $fileName);
					if(count($data) <= 0) return Redirect::to('/home')->withErrors('Data Error !');
					$newData = array();
					$newData[] = 'Username/お名前:,'.Input::get("username");
					$newData[] = 'Address/住所:,'.Input::get("address");
					$newData[] = 'e-mail:,'.Input::get("email");
					$newData[] = '日付,種別,確認コード,開始日,夜	,ゲスト,物件名,物件種別,Address,詳細,紹介文,通貨,額,送金済,ホスト不在,清掃料金';
					foreach ($data as $key => $tmp) {
						$newData[] = $tmp['date'] . "," . $tmp['cate'] . "," . $tmp['code'] . "," . $tmp['start'] . "," . $tmp['num'] . "," . $tmp['name'] . "," . $tmp['estate'] . ",,," . $tmp['detail'] . "," . $tmp['num8'] . "," . $tmp['currency'] . "," . $tmp['price'] . "," . $tmp['pay'] . "," . $tmp['lost'] . "," . $tmp['fee'] . "," . $tmp['id'];
					}
					$editedDir = Config::get('app.edit_dir') . Session::get('filename');
					$content = "";
					//@file_put_contents($editedDir, "\xEF\xBB\xBF". $content );
					$fp = File::put($editedDir, "\xEF\xBB\xBF" . implode("\n", $newData) );
					return Redirect::to('/home/edit');
					
				}
				
			}
		}
		return Redirect::to('/');
	}
	private function getDataFromCSV($fileDir, $type = 'import')
	{
		if(!file_exists( $fileDir ))
		{
			return array();
		}
		$fContent = @file_get_contents($fileDir);
		$rows = explode("\n", $fContent);
		$data = array();
		foreach ($rows as $key=>$row) {
			if(stristr($row, '受取金') || stristr($row, '予約'))
			{
				$tmp = array();
				$line = explode(',', $row);
				if(count($line) < 14) continue;
				if($line[1] != '受取金' && $line[1] != '予約') continue;
				$tmp['date'] = $line[0];
				$tmp['cate'] = $line[1];
				$tmp['code'] = $line[2];
				$tmp['start'] = $line[3];
				$tmp['num'] = $line[4];
				$tmp['name'] = $line[5];
				$tmp['estate'] = $line[6];
				if($type == 'import')
				{
					$tmp['detail'] = $line[7];
					$tmp['num8'] = $line[8];
					$tmp['currency'] = $line[9];
					$tmp['price'] = $line[10];
					$tmp['pay'] = $line[11];
					$tmp['lost'] = $line[12];
					$tmp['fee'] = $line[13];
					$tmp['id'] = $key;
				} else {
					$tmp['ecate'] = $line[7];
					$tmp['eadd'] = $line[8];
					$tmp['detail'] = $line[9];
					$tmp['num8'] = $line[10];
					$tmp['currency'] = $line[11];
					$tmp['price'] = $line[12];
					$tmp['pay'] = $line[13];
					$tmp['lost'] = $line[14];
					$tmp['fee'] = $line[15];
					$tmp['id'] = $key;
				}
				
				$data[] = $tmp;
			}
		}
		return $data;
	}
	public function getEdit()
	{
		if(! Session::get('username'))
		{
			return Redirect::to('/home')->withErrors('Please upload CSV file !');
		}
		$fileDir = Config::get('app.edit_dir') . Session::get('filename');
		if(!file_exists( $fileDir ))
		{
			return Redirect::to('/home')->withErrors('CSV file does not exist !');
		}
		$data = $this->getDataFromCSV($fileDir, 'get');
		if(!Session::get('tableCol')) Session::put('tableCol', array());
		if(count($data) <= 0) return Redirect::to('/home')->withErrors('Data Error !');

		return View::make('pages.edit')->with('csvData', $data);
	}
	public function postSaveEdit()
	{
		$json = array();
		$json['type'] = 'error';
		$json['mess'] = '';
		if(Input::get('ids') && Session::get('filename'))
		{
			$fileDir = Config::get('app.edit_dir') . Session::get('filename');
			if(!file_exists( $fileDir ))
			{
				$json['mess'] = 'File not Found. Please reUpload file !';
			} else {
				$fContent = @file_get_contents($fileDir);
				$rows = explode("\n", $fContent);
				$data = array();
				foreach (Input::get('ids') as $key => $value) {
					if(isset($rows[$value]))
					{
						$line = explode(',', $rows[$value]);
						$line[6] = $this->filterData(Input::get('name'));
						$line[7] = $this->filterData(Input::get('cat'));
						$line[8] = $this->filterData(Input::get('add'));
						$line = implode(',', $line);
						$rows[$value] = $line;
					}
				}
				$editedDir = Config::get('app.edit_dir') . Session::get('filename');
				$newData = implode("\n", $rows);
				if(file_exists($editedDir . $newData)) File::delete($editedDir . $newData);
				$fp = File::put($editedDir, $newData );
				$json['mess'] = 'Save OK !';
				$json['type'] = 'success';
			}
			
		}
		return json_encode($json);
	}
	private function filterData($str)
	{
		return str_replace(',', '.', $str);
	}
	public function getDownload($param = 0)
	{
		if( ! Session::get('filename')) return Redirect::to('/home')->withErrors('Data Error !');
		$fileDir = Config::get('app.edit_dir') . Session::get('filename');
		$data = $this->getDataFromCSV($fileDir, 'get');
		if(count($data) <= 0) return Redirect::to('/home')->withErrors('Data Error !');
		//write file 
		$content = "日付,種別,確認コード,開始日,夜,ゲスト,物件名,物件種別,物件の住所,詳細,紹介文,通貨,額,送金済,ホスト不在,清掃料金\n";

		if($param == 1)
		{
			foreach ($data as $key => $tmp) {
				if($tmp['ecate'] == '賃貸物件')
				$content .= $tmp['date'] . "," . $tmp['cate'] . "," . $tmp['code'] . "," . $tmp['start'] . "," . $tmp['num'] . "," . $tmp['name'] . "," . $tmp['estate'] . "," . $tmp['ecate'] . ",\"" . $tmp['eadd'] . "\"," . $tmp['detail'] . "," . $tmp['num8'] . "," . $tmp['currency'] . "," . $tmp['price'] . "," . $tmp['pay'] . "," . $tmp['lost'] . "," . $tmp['fee'] . "\n";
			}
			$fileName = Config::get('app.down_dir') . "1_" . md5(Session::get('username')) . "_" . date('Y-m-d') . ".csv";
			
		} else {
			foreach ($data as $key => $tmp) {
				if($tmp['ecate'] == '自己所有')
				$content .= $tmp['date'] . "," . $tmp['cate'] . "," . $tmp['code'] . "," . $tmp['start'] . "," . $tmp['num'] . "," . $tmp['name'] . "," . $tmp['estate'] . "," . $tmp['ecate'] . ",\"" . $tmp['eadd'] . "\"," . $tmp['detail'] . "," . $tmp['num8'] . "," . $tmp['currency'] . "," . $tmp['price'] . "," . $tmp['pay'] . "," . $tmp['lost'] . "," . $tmp['fee'] . "\n";
			}
			$fileName = Config::get('app.down_dir') . "2_" . md5(Session::get('username')) . "_" . date('Y-m-d') . ".csv";
			
		}

		File::put($fileName, "\xEF\xBB\xBF" . $content);
		$headers = array(
			  'Content-Type: application/vnd.ms-excel',
			);
		if($param == 1)
			return Response::download($fileName, "不動産所得の収入の内訳_" . Session::get('username') . "_" . date('Y-m-d') . ".csv", $headers);
		else 
			return Response::download($fileName, "所得の内訳所_" . Session::get('username') . "_" . date('Y-m-d') . ".csv", $headers);
	}
	public function getUndoall()
	{
		$data = $this->getDataFromCSV(Config::get('app.upload_dir') . Session::get('filename'));
		if(count($data) <= 0) return Redirect::to('/home')->withErrors('Data Error !');
		$newData = array();
		$newData[] = 'Username/お名前:,'.Session::get("username");
		$newData[] = 'Address/住所:,'.Session::get("address");
		$newData[] = 'e-mail:,'.Session::get("email");
		$newData[] = '日付,種別,確認コード,開始日,夜	,ゲスト,物件名,物件種別,Address,詳細,紹介文,通貨,額,送金済,ホスト不在,清掃料金';
		foreach ($data as $key => $tmp) {
			$newData[] = $tmp['date'] . "," . $tmp['cate'] . "," . $tmp['code'] . "," . $tmp['start'] . "," . $tmp['num'] . "," . $tmp['name'] . "," . $tmp['estate'] . ",,," . $tmp['detail'] . "," . $tmp['num8'] . "," . $tmp['currency'] . "," . $tmp['price'] . "," . $tmp['pay'] . "," . $tmp['lost'] . "," . $tmp['fee'] . "," . $tmp['id'];
		}
		$editedDir = Config::get('app.edit_dir') . Session::get('filename');
		$content = "";
		//@file_put_contents($editedDir, "\xEF\xBB\xBF". $content );
		$fp = File::put($editedDir, "\xEF\xBB\xBF" . implode("\n", $newData) );
		return Redirect::to('/home/edit');
	}

	public function getExport($type = 1)
	{
		if($type == 1) {
			$rates = $this->getRates();
			$data = $this->getEditFileData();
			$newData = $this->exportData1($data);
			if(count($newData) <= 0) return Redirect::to('/home')->withErrors('Data Error !');

			return View::make('pages.export')->with('csvData', $newData)->withRates($rates);
		} else {
			$rates = $this->getRates();
			$data = $this->getEditFileData();
			$newData = $this->exportData1($data, $type);
			if(count($newData) <= 0) return Redirect::to('/home')->withErrors('Data Error !');

			return View::make('pages.export1')->with('csvData', $newData)->withRates($rates);
		}
		
	}
	public function getEditFileData()
	{
		if(! Session::get('username'))
		{
			return Redirect::to('/home')->withErrors('Please upload CSV file !');
		}
		$fileDir = Config::get('app.edit_dir') . Session::get('filename');
		if(!file_exists( $fileDir ))
		{
			return Redirect::to('/home')->withErrors('CSV file does not exist !');
		}
		return $this->getDataFromCSV($fileDir, 'get');
	}
	public function getDownloadXlsx($type = 1)
	{
		if(! Session::get('username'))
		{
			return Redirect::to('/home')->withErrors('Please upload CSV file !');
		}
		if($type == 1)
		{

			$fileName = "不動産所得の収入の内訳_" . Session::get('username') . "_" . date('Y-m-d');
			Excel::create($fileName, function($excel) {
				$excel->sheet('Sheet 1', function($sheet) {
					$sheet->row(1, array('不動産収入の内訳所'));
					$sheet->cells('A1:K1', function($cells) {
						$cells->setFontSize(20);
						$cells->setAlignment('center');
					});
					$sheet->mergeCells('A1:K1');
					$sheet->row(2, array('氏名', Session::get('username')));
					$sheet->row(3, array('e-mail', Session::get('email')));
					$sheet->row(4, array('住所', Session::get('address')));
					$sheet->cells('A2:A4', function($cells) {
						$cells->setBackground('#dff0d8');
					});
					$sheet->mergeCells('B2:C2');
					$sheet->mergeCells('B3:C3');
					$sheet->mergeCells('B4:C4');
					$sheet->setBorder('A2:C4', 'thin');
					$sheet->row(6, array(
					    '貸家貸地等の別', '用途（住宅用、住宅用以外等の別）', '不動産の所在地', '賃借人の住所・氏名',
					    '賃貸契約期間', '貸付面積', '本年中の収入金額', '', '', '', '補償金敷金（期末残高）' 
					));
					$sheet->row(7, array(
					    '', '', '', '', '', '', '賃貸料', '', '礼金権利金更新料', '名義書換料その他' 
					));
					$sheet->row(8, array(
						'', '', '', '', '', '', '月額', '年額' 
					));
					$sheet->mergeCells('A6:A8');
					$sheet->mergeCells('B6:B8');
					$sheet->mergeCells('C6:C8');
					$sheet->mergeCells('D6:D8');
					$sheet->mergeCells('E6:E8');
					$sheet->mergeCells('F6:F8');
					$sheet->mergeCells('G6:J6');
					$sheet->mergeCells('G7:H7');
					$sheet->mergeCells('I7:I8');
					$sheet->mergeCells('J7:J8');
					$sheet->mergeCells('K6:K8');
					$sheet->row(6, function($row) { $row->setBackground('#dff0d8'); });
					$sheet->row(7, function($row) { $row->setBackground('#dff0d8'); });
					$sheet->row(8, function($row) { $row->setBackground('#dff0d8'); });
					
					
					//$sheet->setAutoSize(true);
					$data = $this->getEditFileData();

					$i = 9;
					$newData = $this->exportData1($data, 1);
					$totalMoney = 0;
					if(count($newData) > 0) {
						foreach ($newData as $v) {
							$sheet->row($i, array(
								$v['col1'], $v['col2'], $v['col3'], $v['col4'],
								str_replace("<br>", "\n", $v['col5']), $v['col6'], $v['col7'], $v['col8'],
								' ', ' ', ' '
							));
							$i += 1;
							$totalMoney += $v['col8'];
						}
						$sheet->getStyle('C'.$i)->getAlignment()->setWrapText(true);
					}
					$sheet->setWidth(array(
						'A'=>15,'B'=>15,'C'=>50,'D'=>10,'E'=>10,'F'=>10,'G'=>10,'H'=>15,'I'=>10,'J'=>10,'K'=>10
					));
					for ($j=9; $j < $i ; $j++) { 
						$sheet->setHeight($j, 30);
					}
					$sheet->setBorder('A6:K'.$i, 'thin');
					$sheet->row($i, function($row) { $row->setBackground('#dff0d8'); $row->setFontWeight('bold'); });
					$sheet->row($i, array('計', ' ', ' ', ' ', ' ', ' ', ' ', $totalMoney));
					//$sheet->setBorder('B'.$i.':G'.$i, );
					$sheet->setColumnFormat(array(
						'H9:H'.$i => '#,##'
					));
			    });
			})->download('xlsx');
		} else {

			$fileName = "所得の内訳所_" . Session::get('username') . "_" . date('Y-m-d');
			Excel::create($fileName, function($excel) {
				$excel->sheet('Sheet 1', function($sheet) {
					$sheet->row(1, array('所得の内訳所'));
					$sheet->cells('A1:G1', function($cells) {
						$cells->setFontSize(20);
						$cells->setAlignment('center');
					});
					$sheet->mergeCells('A1:G1');
					$sheet->row(2, array('氏名', Session::get('username')));
					$sheet->row(3, array('e-mail', Session::get('email')));
					$sheet->row(4, array('住所', Session::get('address')));
					$sheet->cells('A2:A4', function($cells) {
						$cells->setBackground('#dff0d8');
					});
					$sheet->mergeCells('B2:C2');
					$sheet->mergeCells('B3:C3');
					$sheet->mergeCells('B4:C4');
					$sheet->setBorder('A2:C4', 'thin');

					$sheet->row(6, array(
					    '所得の種類', '種目', '所得の生ずる場所又は給与などの支払者の住所・所在地、氏名・名称、電話番号', '所得の基因となる資産の数量',
					    '収入金額（所得税及び復興特別所得税の源泉徴収税額を差し引かれる前の収入金額）',
					    '所得税及び復興特別所得税の源泉徴収税額',
					    '支払確定年月又は支払を受けた年月' 
					));
					$sheet->row(6, function($row) { $row->setBackground('#dff0d8'); });
					$data = $this->getEditFileData();
					$i = 7;
					$newData = $this->exportData1($data, 2);
					$totalMoney = 0;
					if(count($newData) > 0) {
						foreach ($newData as $v) {
							$sheet->row($i, array(
								$v['col1'], $v['col2'], str_replace("<br>", "\r\n", $v['col3']), $v['col4'],
								$v['col5'], $v['col6'], str_replace("<br>", "\r\n", $v['col7'])
							));
							$i += 1;
							$totalMoney += $v['col5'];
						}
						$sheet->getStyle('C'.$i)->getAlignment()->setWrapText(true);
					}
					$sheet->setWidth(array(
						'A'=>15,'B'=>15,'C'=>50,'D'=>10,'E'=>15,'F'=>10,'G'=>10
					));
					for ($j=7; $j < $i ; $j++) { 
						$sheet->setHeight($j, 30);
					}
					$sheet->setBorder('A7:G'.$i, 'thin');
					$sheet->row($i, function($row) { $row->setBackground('#dff0d8'); $row->setFontWeight('bold'); });
					$sheet->row($i, array('計 ', ' ', ' ', ' ', $totalMoney));
					$sheet->setColumnFormat(array(
						'E7:E'.$i => '#,##'
					));
			    });
			})->download('xlsx');
		}
	}
	private function exportData1($data, $type = 1)
	{
		if($type == 1) {
			$rates = $this->getRates();
			$newData = $tmp = $checkArr = array();
			foreach ($data as $value) {
				if(strlen($value['start']) == 10 
					&& !in_array($value['estate'].$value['ecate'].$value['eadd'], $checkArr) 
					&& in_array($value['ecate'], array('アパート', 'マンション', '貸家') ))
				{
					$tmp['col1'] = $value['ecate'];
					$tmp['col2'] = '住宅用以外';
					$tmp['col3'] = $value['eadd'] . " \n" . $value['estate'];
					$tmp['col4'] = '';
					$fromDate = time();
					$toDate = 0;
					$totalMoney = 0;
					foreach ($data as $val) {
						if($val['estate'] == $value['estate'] && $val['ecate'] == $value['ecate'] && $val['eadd'] == $value['eadd'])
						{
							if($fromDate > strtotime($val['start']))
							{
								$fromDate = strtotime($val['start']);
							}
							if($toDate < strtotime($val['start']))
							{
								$toDate = strtotime($val['start']);
							}
							if($val['currency'] == 'JPY') {
								$totalMoney += $val['price']; //CAN TINH LAI JPY & USD
							} else if($val['currency'] == 'USD')
							{
								$yearvsmonth = date('Y', strtotime($val['start']) ) . '/' . date('m', strtotime($val['start']) );
								if(isset($rates[$yearvsmonth])){
									$totalMoney += round(round($val['price']) * $rates[$yearvsmonth]);
								}
							}

							$checkArr[] = $value['estate'].$value['ecate'].$value['eadd'];
						}
					}
					$from_jpyear = date('Y', $fromDate) - 1988;
					$from_month = date('m', $fromDate);
					$to_jpyear = date('Y', $toDate) - 1988;
					$to_month = date('m', $toDate);
					$tmp['col5'] = '自' . $from_jpyear . '．' . $from_month . '<br>至' . $to_jpyear . '．' . $to_month;//$fromDate . '==' . $toDate; // ngay nhat
					$tmp['col6'] = '';
					$tmp['col7'] = '';
					$tmp['col8'] = $totalMoney;
					$newData[] = $tmp;
				}
				
			}
			return $newData;
		} else {
			$rates = $this->getRates();
			$newData = $tmp = $checkArr = array();
			foreach ($data as $value) {
				if(strlen($value['start']) == 10 && !in_array($value['estate'].$value['ecate'].$value['eadd'], $checkArr) && $value['ecate'] == '自己所有' )
				{
					$tmp['col1'] = '雑（その他）';
					$tmp['col2'] = '賃貸料';
					$tmp['col3'] = $value['eadd'] . " \n" . $value['estate'];
					$tmp['col4'] = '';
					$fromDate = time();
					$toDate = 0;
					$totalMoney = 0;
					foreach ($data as $val) {
						if($val['estate'] == $value['estate'] && $val['ecate'] == $value['ecate'] && $val['eadd'] == $value['eadd'])
						{
							if($fromDate > strtotime($val['start']))
							{
								$fromDate = strtotime($val['start']);
							}
							if($toDate < strtotime($val['start']))
							{
								$toDate = strtotime($val['start']);
							}
							if($val['currency'] == 'JPY') {
								$totalMoney += $val['price']; //CAN TINH LAI JPY & USD
							} else if($val['currency'] == 'USD')
							{
								$yearvsmonth = date('Y', strtotime($val['start']) ) . '/' . date('m', strtotime($val['start']) );
								if(isset($rates[$yearvsmonth])){
									$totalMoney += round(round($val['price']) * $rates[$yearvsmonth]);
								}
							}

							$checkArr[] = $value['estate'].$value['ecate'].$value['eadd'];
						}
					}
					$from_jpyear = date('Y', $fromDate) - 1988;
					$from_month = date('m', $fromDate);
					$to_jpyear = date('Y', $toDate) - 1988;
					$to_month = date('m', $toDate);
					$tmp['col5'] = $totalMoney;
					$tmp['col6'] = '';
					$tmp['col7'] = '自' . $from_jpyear . '．' . $from_month . '<br>至' . $to_jpyear . '．' . $to_month;//$fromDate . '==' . $toDate; // ngay nhat
					
					$newData[] = $tmp;
				}
				
			}
			return $newData;
			
		}
	}
	private function isDate($str){ 
		$str = str_replace('/', '-', $str);	 
		$stamp = strtotime($str);
		if (is_numeric($stamp)){ 
			$month = date( 'm', $stamp ); 
			$day   = date( 'd', $stamp ); 
			$year  = date( 'Y', $stamp ); 
			return checkdate($month, $day, $year); 
		}  
		return false; 
	}
	public function postSendContact()
	{
		$data['type'] = 'danger';
		$data['message'] = 'ERROR !';
		if(Input::get('name') && Input::get('email') && Input::get('message'))
		{
			Mail::send('emails.response_support', array('name'=>Input::get('name'), 'messages'=>Input::get('message')), function($message){
			    $message->to(Input::get('email'), Input::get('name'))->subject('【ACE】ご要望を受け付けました。');
			});
			$data['type'] = 'success';
			$data['message'] = 'この度はAirbnb CSV Editorへご要望をお送り頂き誠にありがとうございます。';

			Mail::send('emails.webmaster', array('name'=>Input::get('name'), 'title'=>Input::get('title'), 'email'=>Input::get('email'), 'messages'=>Input::get('message')), function($message){
			    $message->to(Config::get('app.webmaster_email'), 'Webmaster')->subject('Airbnb Support');
			});
			
		}
		echo json_encode($data);
	}
	public function postSetCol()
	{
		Session::put('tableCol', Input::get('col'));
		return Redirect::back();
	}
	public function getChangeLog()
	{
		return View::make('pages.changelog');
	}
	public function getGuide()
	{
		return View::make('pages.guide');
	}
}
