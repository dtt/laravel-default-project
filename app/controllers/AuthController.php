<?php 

class AuthController extends Controller {
	
	public function login()
	{
		return View::make('admin.login');
	}
	public function doLogin()
	{
		$validator = new App\DTT\Forms\UserLoginForm;
		$data['has'] = false;
		if($validator->fails())
		{
			if (Auth::attempt(array('username' => Input::get('username'), 'password' => Input::get('password'))))
			{
			    $data['has'] = true;
				return Response::json($data);
			}
			return Response::json($data);
		} else {
			$data['has'] = true;
			return Response::json($data);
		}
	}

}