<?php 

class AdminController extends Controller {
	
	public function getIndex()
	{
		return View::make('admin.dashboard');
	}
	public function getLogout()
	{
		Auth::logout();
		return Redirect::to('/');
	}
	public function getAdsManager($params = false, $id = false)
	{
		if($params == 'add') return $this->adsAdd();
		if($params == 'edit') return $this->adsEdit($id);
		if($params == 'delete') return $this->adsDelete($id);
		$ads = Ads::orderBy('id', 'DESC')->paginate(10);
		return View::make('admin.ads.manager', compact('ads'));
	}
	public function postAdsManager($params = false, $id = false)
	{
		//handle form submit
		if($params == 'doAdd') return $this->adsDoAdd();
		if($params == 'doEdit') return $this->adsDoEdit($id);
	}
	protected function adsAdd()
	{
		return View::make('admin.ads.add');
	}
	protected function adsDoAdd()
	{
		$validate = Validator::make(Input::all(), ['codes'=>'required']);
		if($validate->fails())
		{
			return Redirect::to('admin/ads-manager/add')->withInput()->withErrors($validate);
		} else {
			Ads::create([
				'code'	=>	Input::get('codes'),
				'position'	=>	Input::get('position'),
				]);
			return Redirect::to('admin/ads-manager/add')->withSuccess('Your Ads has been added successfully !');
		}
	}
	protected function adsDoEdit($id = false)
	{
		$ads = Ads::find($id);
		if(!$ads) return Redirect::back()->withErrors('Could not find ADS !');
		$ads->code = Input::get('codes');
		$ads->position = Input::get('position');
		$ads->save();
		return Redirect::to('admin/ads-manager')->withSuccess('Update Ads successfully');
	}
	protected function adsEdit($id = false)
	{
		$ads = Ads::find($id);
		if(!$ads) return Redirect::back()->withErrors('Could not find ADS !');

		return View::make('admin.ads.edit', compact('ads'));
	}
	protected function adsDelete($id)
	{
		Ads::where('id', $id)->delete();
		return Redirect::to('admin/ads-manager')->withSuccess('Delete ads successfully');
	}
	public function getCsv($page = 1, $show = 20)
	{
		$files_upload = $this->getFilesUpload();
		for($i = 0; $i < count($files_upload); $i++ )
		{
			for($j = $i + 1; $j < count($files_upload); $j++ )
			{
				if($files_upload[$i] > $files_upload[$j])
				{
					$tmp = $files_upload[$i];
					$files_upload[$i] = $files_upload[$j];
					$files_upload[$j] = $tmp;
				}
			}
		}
		//paginate
		$offset = ($page * $show ) - $show;
		$show_files = array();
		foreach ($files_upload as $k=>$file) {
			if( ($k+1)>=$offset && ($k+1)<=($offset+$show) )
			{
				$show_files[] = $file;
			}
		}
		
		$paginate = App\DTT\Paginate\FilePagination::make($page, ceil(count($files_upload)/$show), URL::to('admin/csv/%s/'.$show) );
		$files_upload = $show_files;
		return View::make('admin.csv.manager', compact('files_upload', 'paginate'));
	}
	private function getFilesUpload()
	{
		$upload_dir = Config::get('app.upload_dir');
		$edit_dir = Config::get('app.edit_dir');
		$dh  = opendir($upload_dir);
		$files_upload = array();
		while (false !== ($filename = readdir($dh))) {
			if($filename == '.' || $filename == '..') continue;
			$line = explode('_', $filename);
			$files_upload[] = array(
				'user_name' => base64_decode($line[0]),
				'date_day' => @$line[1],
				'date_hour' => @$line[2],
				'new_fname' => base64_decode($line[0]) .'_'. @$line[1] .'_'. @$line[2],
				'file_name' => $filename,
				);
		}
		return $files_upload;
	}
	public function getCsvDel($filename = false)
	{
		$upload_dir = Config::get('app.upload_dir');
		$file = new Illuminate\Filesystem\Filesystem;
		$file->delete($upload_dir . $filename);
		return Redirect::to('admin/csv')->withSuccess('File deleted successfully');
	}
	public function postDelCsv()
	{
		$upload_dir = Config::get('app.upload_dir');
		if(count(Input::get('child-cb')))
		{
			$files = array();
			foreach (Input::get('child-cb') as $key => $value) {
				$files[] = $upload_dir . $value;
			}
			$file = new Illuminate\Filesystem\Filesystem;
			$file->delete($files);
			return Redirect::to('admin/csv')->withSuccess('Files deleted successfully');
		}
	}
	public function getDiskspace()
	{
		$sv['free_disk'] = disk_free_space(base_path());
		$sv['total_disk'] = disk_total_space(base_path());

		$sv['used'] = $sv['total_disk'] - $sv['free_disk'];
		$sv['percent_used'] = ceil($sv['used']/$sv['total_disk'] * 100);

		$sv['percent'] = 100 - $sv['percent_used'];

		$sv['free_disk'] = $this->formatBytes($sv['free_disk']);
		$sv['total_disk'] = $this->formatBytes($sv['total_disk']);
		$sv['used'] = $this->formatBytes($sv['used']);
		return View::make('admin.diskspace', compact('sv'));
	}
	public function getCsvDownload()
	{
		return View::make('admin.csv.download');
	}
	public function postCsvDownload()
	{
		$upload_dir = Config::get('app.upload_dir');
		$months = array();
		foreach (Input::get('dateM') as $mm) {
			$months[] = str_pad($mm, 2, '0', STR_PAD_LEFT);
		}
		$year = Input::get('dateY');
		$files = $this->getFilesUpload();
		$fileDownload = array();
		foreach ($files as $file) {
			foreach($months as $month) {
				if(stristr($file['date_day'], $year . '-' . $month)) {
					$fileDownload[] = $upload_dir . $file['file_name'];
				}
			}
			
		}
		$store_dir = base_path() . '/uploads/tmp/'.$year.'-'.implode("_",$months).'.zip';
		Zipper::make($store_dir)->add($fileDownload);
		sleep(1);
		return Response::download('uploads/tmp/'.$year.'-'.implode("_",$months).'.zip', $year.'-'.implode("_",$months).'.zip');

	}
	private function formatBytes($size, $precision = 0)
	{
		$base = log($size, 1024);
		$suffixes = array('', 'KB', 'MB', 'GB', 'TB');   

		return round(pow(1024, $base - floor($base)), $precision) .' '. $suffixes[floor($base)];
	}
}